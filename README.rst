
This repository comprises a machine-readable representation of known AWOS
stations in Colorado as determined from the CDOT page on 
"`Automated Weather Stations
<https://www.codot.gov/programs/aeronautics/co_awos>`_", suitable for loading
into `CHIRP <https://chirp.danplanet.com/projects/chirp/wiki/Home>_`.

Files
-----

AWOSCard.pdf
   Downloaded from https://www.codot.gov/programs/aeronautics/PDF_Files/AWOSCard
   (2018-07-24)

carddata.txt
   Results of running ``pdf2txt.py`` on ``AWOSCard.pdf``, followed by some
   manual cleanup.

script.py
   Script for translating ``carddata.txt`` into ``co_awos.csv``.
   Format of output is subject to change, and there are a few parameters
   parsed which are not output, but this may be useful later.

co_awos.csv
   Output file suitable for input to CHIRP.
