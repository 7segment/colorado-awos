import csv
import re
from decimal import Decimal

N = 60 # Total number of stations
RE = re.compile('(.+)\((.+)\)')

locations = []
latlongs = []
freqs = []
phones = []

with open('carddata.txt', 'r') as infile:
    for i, line in enumerate(infile.readlines()):
        if i < N:
            locations.append(line.strip())
        elif N <= i < N*2:
            latlongs.append(line.strip())
        elif N*2 <= i < N*3:
            freqs.append(line.strip())
        elif N*3 <= i < N*4:
            phones.append(line.strip())
        else:
            raise RuntimeError('expected {} entries, found more'.format(N))

class Station(object):
    def __init__(self, location, latlong, freq, phone):
        self.name, self.id = RE.match(location).groups()
        self.latlong = latlong
        self.freq = Decimal(freq)
        self.phone = phone

    def csvrow(self, i):
        return [i,
                self.id,
                self.freq,
                'AM',
                '{} ({})'.format(self.name, self.latlong)]

with open('co_awos.csv', 'w') as outfile:
    writer = csv.writer(outfile)
    writer.writerow(['Location',
                     'Name',
                     'Frequency',
                     'Mode',
                     'Comment'])
    for i, stn in enumerate(Station(*z) for z in zip(locations,
                                                     latlongs,
                                                     freqs,
                                                     phones)):
        writer.writerow(stn.csvrow(i+1))
